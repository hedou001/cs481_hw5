﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW5
{
    class PinModel
    {
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
