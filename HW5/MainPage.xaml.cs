﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        List<PinModel> pins = new List<PinModel>();
        public MainPage()
        {
            InitializeComponent();
            // Populating pins
            pins.Add(new PinModel { Name = "FR North Home", Longitude = 50.194465, Latitude = 1.515664 });
            pins.Add(new PinModel { Name = "FR Paris Home", Longitude = 48.812149, Latitude = 2.355895 });
            pins.Add(new PinModel { Name = "FR Sail Club", Longitude = 50.342847, Latitude = 1.551114 });
            pins.Add(new PinModel { Name = "FR Work Place", Longitude = 48.858250, Latitude = 2.294208 });

            // Populating PinPicker
            PinPicker.ItemsSource = pins;
            // Putting pins on the map
            DisplayPinsOnMap(pins);
        }

        private void DisplayPinsOnMap(List<PinModel> pins)
        {
            // Create special pin items for the map element
            foreach(PinModel elem in pins)
            {
                var pin = new Xamarin.Forms.Maps.Pin();
                pin.Label = elem.Name;
                pin.Type = Xamarin.Forms.Maps.PinType.SavedPin;
                pin.Position = new Xamarin.Forms.Maps.Position(elem.Longitude, elem.Latitude);
                MyMap.Pins.Add(pin);
            }
        }

        // Map Style Picker
        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                if (picker.Items[selectedIndex] == "Satellite")
                    MyMap.MapType = Xamarin.Forms.Maps.MapType.Satellite;
                else if (picker.Items[selectedIndex] == "Street")
                    MyMap.MapType = Xamarin.Forms.Maps.MapType.Street;
                else if (picker.Items[selectedIndex] == "Hybrid")
                    MyMap.MapType = Xamarin.Forms.Maps.MapType.Hybrid;
            }
        }

        // Pins Picker
        private void Picker_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                // Move to pin's position
                PinModel pin = pins[selectedIndex];
                Xamarin.Forms.Maps.MapSpan span = Xamarin.Forms.Maps.MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(pin.Longitude, pin.Latitude), Xamarin.Forms.Maps.Distance.FromKilometers(10.0f));
                MyMap.MoveToRegion(span);
            }
        }
    }
}
